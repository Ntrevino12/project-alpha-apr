from django.shortcuts import render, redirect
from projects.models import Project
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectsForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": projects}
    return render(request, "projects/list.html", context)


def show_project(request, id):
    projects = Project.objects.filter(owner=request.user, id=id)
    tasks = Task.objects.all()
    context = {
        "list_projects": projects,
        "show_project": tasks,
    }
    return render(request, "projects/detail.html", context)


def create_project(request):
    if request.method == "POST":
        form = ProjectsForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectsForm()
    context = {
        "form": form,
    }

    return render(request, "projects/create.html", context)
