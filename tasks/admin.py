from django.contrib import admin
from tasks.models import Task


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    lsit_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "id",
    )


# Register your models here.
